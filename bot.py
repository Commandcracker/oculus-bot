#!/usr/bin/python3
# -*- coding: utf-8 -*-

# built-in modules
import random
import time
import math
import json
from os import getenv


# pip modules
import aiohttp
import discord
from discord.ext import commands
from discord_slash import SlashCommand, SlashContext
from rich.console import Console
from rich.text import Text
from rich.traceback import Traceback, install
console = Console()


class Oculus_Bot(object):
    def __init__(self, token, prefix, owners, announcements_ids):
        self.token = token
        self.owners = owners
        self.announcements_ids = announcements_ids
        self.prefix = prefix
        self.bot = commands.Bot(self.prefix, case_insensitive=True)
        self.bot.remove_command('help')
        self.slash = SlashCommand(self.bot, sync_commands=True)

    def run(self):
        @self.bot.event
        async def on_ready():
            await self.bot.change_presence(
                status=discord.Status.online,
                activity=discord.Game(name=self.prefix + 'help', type=2)
            )
            console.print(
                Text.assemble(
                    ("\n"),
                    (" =======- ", "bright_green"),
                    (time.strftime("%d.%m.%Y"), "green"),
                    (" -=======\n", "bright_green"),
                    ("  Bot Name: ", "bright_green"),
                    (self.bot.user.name + '#' +
                     str(self.bot.user.discriminator) + "\n", "bright_yellow"),
                    ("  Bot ID: ", "bright_green"),
                    (str(self.bot.user.id) + "\n", "bright_yellow"),
                    ("  Discord Version: ", "bright_green"),
                    (str(discord.__version__) + "\n", "bright_yellow"),
                    (" ========- ", "bright_green"), (time.strftime("%H:%M:%S"), "green"),
                    (" -========", "bright_green"),
                    ("\n")
                )
            )

        @self.bot.event
        async def on_slash_command_error(ctx: SlashContext, error: Exception):
            await on_command_error(ctx, error)

        @self.bot.event
        async def on_command_error(ctx: SlashContext, error: Exception):
            # This prevents any commands with local handlers being handled here in on_command_error.
            if hasattr(ctx.command, 'on_error'):
                return

            # get the original exception
            error = getattr(error, 'original', error)

            if isinstance(error, commands.CommandNotFound):
                return

            if isinstance(error, commands.NoPrivateMessage):
                try:
                    await ctx.send("""```diff
- """ + 'The command "' + str(ctx.command) + '" can not be used in Private Messages.' + "```")
                except discord.HTTPException:
                    return
                return

            if isinstance(error, discord.Forbidden):
                try:
                    await ctx.send("""```diff
- """ + ' Missing Permissions' + "```")
                except discord.Forbidden:
                    return
                return

            if isinstance(error, commands.UserInputError):
                await ctx.send("""```diff
- Usage: """ + self.prefix + str(ctx.command) + " " + ctx.command.usage + "```")
                return

            traceback = Traceback.from_exception(
                type(error), error, error.__traceback__)
            console.print(traceback)

        @self.bot.command(name='say', description='force to bot to say something', usage="[message]")
        async def _say(ctx: SlashContext, *, args):
            if ctx.author.id in self.owners:
                await ctx.send(args)

        @discord.ext.commands.guild_only()
        @self.bot.command(name='scan', description='get all channels in the server')
        async def _scan(ctx: SlashContext):
            if ctx.author.id in self.owners:
                channels = ctx.guild.channels
                channels.sort(key=lambda c: (
                    c._sorting_bucket, c.position, c.id))
                for channel in channels:
                    if not channel.category and not isinstance(channel, discord.CategoryChannel):
                        if isinstance(channel, discord.VoiceChannel):
                            await ctx.send("`{}`".format(str(channel.name)))
                        if isinstance(channel, discord.TextChannel):
                            await ctx.send("`# {}`".format(str(channel.name)))
                    if isinstance(channel, discord.CategoryChannel):
                        await ctx.send("`⯆ {}`".format(str(channel.name)))
                        for c in channel.channels:
                            if isinstance(c, discord.VoiceChannel):
                                await ctx.send("`   {}`".format(str(c.name)))
                            if isinstance(c, discord.TextChannel):
                                await ctx.send("`   # {}`".format(str(c.name)))

        @self.bot.command(name='setstatus', description='set the bot status', usage="[type]")
        async def _setstatus(ctx: SlashContext, type):
            if ctx.author.id in self.owners:
                embed = discord.Embed(color=discord.Color.red())

                embed.add_field(name="online", value="0", inline=True)
                embed.add_field(name="offline", value="1", inline=True)
                embed.add_field(name="idle", value="2", inline=True)
                embed.add_field(name="do not disturb", value="3", inline=True)

                try:
                    type = int(type)
                except ValueError:
                    await ctx.send("**You need to enter an number**", embed=embed)

                else:
                    if type == 0:
                        await self.bot.change_presence(status=discord.Status.online)
                        await ctx.send("**Status: online**")
                    elif type == 1:
                        await self.bot.change_presence(status=discord.Status.offline)
                        await ctx.send("**Status: offline**")
                    elif type == 2:
                        await self.bot.change_presence(status=discord.Status.idle)
                        await ctx.send("**Status: idle**")
                    elif type == 3:
                        await self.bot.change_presence(status=discord.Status.dnd)
                        await ctx.send("**Status: do not disturb**")
                    else:
                        await ctx.send(embed=embed)

        @self.bot.command(name='spam', description='spam a text', usage="[message]")
        async def _spam(ctx: SlashContext, *, args):
            if ctx.author.id in self.owners:
                for i in range(0, 10):
                    await ctx.send(args)

        @discord.ext.commands.guild_only()
        @self.bot.command(name='clear', description='clear text in a channel', usage="[number]")
        async def _clear(ctx: SlashContext, number):
            if ctx.author.id in self.owners:
                try:
                    number = int(number)
                except ValueError:
                    await ctx.send("**You need to enter an number**")
                    return
                if number <= 0 or number > 100:
                    embed = discord.Embed(color=discord.Color.red())

                    embed.add_field(name="max", value="100", inline=True)
                    embed.add_field(name="min", value="1", inline=True)

                    await ctx.send(embed=embed)

                else:
                    await ctx.message.delete()
                    deleted = await ctx.channel.purge(limit=number)

                    embed = discord.Embed(
                        title='Deleted {} message(s)'.format(len(deleted)),
                        color=discord.Color.red()
                    )

                    await ctx.channel.send(delete_after=3, embed=embed)

        @self.bot.command(name="help-dev", aliases=["help_dev"], description="Get a list of all dev-commands")
        async def _help_dev(ctx: SlashContext):
            if ctx.message.author.id in self.owners:
                embed = discord.Embed(color=discord.Color.red())

                embed.set_author(name='Commands-dev')
                embed.add_field(
                    name=self.prefix + 'help-dev',
                    value="Get a list of all dev-commands",
                    inline=False
                )
                embed.add_field(
                    name=self.prefix + 'say',
                    value='force to bot to say something',
                    inline=False
                )
                embed.add_field(
                    name=self.prefix + 'spam',
                    value='spam a text',
                    inline=False
                )
                embed.add_field(
                    name=self.prefix + 'clear',
                    value='clear text in a channel',
                    inline=False
                )
                embed.add_field(
                    name=self.prefix + 'scan',
                    value='get all channels in the server',
                    inline=False
                )
                embed.add_field(
                    name=self.prefix + 'setstatus',
                    value='set the bot status',
                    inline=False
                )

                try:
                    await ctx.message.author.send(embed=embed)
                except discord.Forbidden:
                    try:
                        await ctx.send("**I could not send you an pm**")
                    except discord.Forbidden:
                        pass

        @self.bot.event
        async def on_member_join(member):
            if str(member.guild.id) in self.announcements_ids:
                try:
                    await self.bot.get_channel(self.announcements_ids[str(member.guild.id)]).send(
                        member.mention + ' Joined the Server!')
                except discord.Forbidden:
                    pass

        @self.bot.event
        async def on_member_remove(member):
            if str(member.guild.id) in self.announcements_ids:
                await self.bot.get_channel(
                    self.announcements_ids[str(member.guild.id)]
                ).send(member.mention + ' left the Server!')

        # commands

        @self.slash.slash(name="cat", description="Get a random image of a cat", options=[])
        @self.bot.command(name="cat", description="Get a random image of a cat")
        async def _cat(ctx: SlashContext):
            async with aiohttp.ClientSession() as session:
                async with session.get('http://aws.random.cat/meow') as r:
                    if r.status == 200:
                        js = await r.json()
                        url = js['file']
                        embed = discord.Embed(
                            color=discord.Color.from_rgb(
                                random.randint(1, 255),
                                random.randint(1, 255),
                                random.randint(1, 255)
                            )
                        )
                        embed.set_image(url=url)
                        embed.set_author(name="Cat")
                        await ctx.send(embed=embed)
                    else:
                        await ctx.send("""```diff
- Error: """ + str(r.status) + " " + str(r.reason) + "```")

        @self.slash.slash(name="ping", description="See the ping of the bot", options=[])
        @self.bot.command(name="ping", description="See the ping of the bot")
        async def _ping(ctx: SlashContext):
            time_then = time.monotonic()
            pinger = await ctx.send('Pinging...')
            ping = '%.2f' % (1000 * (time.monotonic() - time_then))
            await pinger.edit(content='Pong! `' + str(round(int(float(ping)))) + 'ms`')

        @self.slash.slash(name="pi", description="Get the amount of pi", options=[])
        @self.bot.command(name="pi", description="Get the amount of pi")
        async def _pi(ctx: SlashContext):
            await ctx.send(content="**π ~ **``" + str(math.pi) + "``")

        @self.slash.slash(name="periodic_system", description="Shows a image of the periodic system", options=[])
        @self.bot.command(name="periodic_system", aliases=['periodic', 'pse', 'ps'], description="Shows a image of the periodic system")
        async def _periodic_system(ctx: SlashContext):
            embed = discord.Embed(color=discord.Color.red())
            embed.set_author(name='Periodic System')
            embed.set_image(
                url="https://upload.wikimedia.org/wikipedia/commons/thumb/0/00/Periodensystem_Einfach.svg/2100px-Periodensystem_Einfach.svg.png")
            await ctx.send(embed=embed)

        @self.slash.slash(name="help", description="Get a list of all commands", options=[])
        @self.bot.command(name="help", aliases=['commands'], description="Get a list of all commands")
        async def _help(ctx: SlashContext):
            embed = discord.Embed(color=discord.Color.red())

            embed.set_author(name='Commands')
            embed.add_field(
                name=prefix + 'help',
                value='Get a list of all commands',
                inline=False
            )
            embed.add_field(
                name=prefix + 'ping',
                value='see ping',
                inline=False
            )
            embed.add_field(
                name=prefix + 'cat',
                value='Get a random image of a cat',
                inline=False
            )
            embed.add_field(
                name=prefix + 'pi',
                value='Get the amount of pi',
                inline=False
            )
            embed.add_field(
                name=prefix + 'periodic_system',
                value='Shows a image of the periodic system',
                inline=False
            )

            await ctx.send(embed=embed)

        # run
        self.bot.run(self.token)


if __name__ == '__main__':
    install(console=console)
    prefix = '!'
    token = getenv("TOKEN")
    if token is None:
        try:
            token = open(r'token.txt', 'r').read()
        except FileNotFoundError:
            console.print("Token not found", style="red")
            exit()

    try:
        owners = json.loads(open("owners.json", "r").read())
    except json.decoder.JSONDecodeError:
        console.print("your owners.json in invalid", "red")
        exit()
    except UnicodeDecodeError:
        console.print("owners.json has a false Decoding", "red")
        exit()
    except FileNotFoundError:
        console.print(
            "You need to create a file called owners.json with the content:",
            style="red"
        )
        console.print("""[
    375312377492668418
]
//  owner client id""")
        exit()

    try:
        announcements_ids = json.loads(
            open("announcements_ids.json", "r").read()
        )
    except json.decoder.JSONDecodeError:
        console.print("your announcements_ids.json in invalid", "red")
        exit()
    except UnicodeDecodeError:
        console.print("announcements_ids.json has a false Decoding", "red")
        exit()
    except FileNotFoundError:
        console.print(
            "You need to create a file called announcements_ids.json with the content:",
            style="red"
        )
        console.print("""{
    "503969457945837589": 533256092575072267
}
//    "server id": "channel id" """)
        exit()

    try:
        Oculus_Bot(token, prefix, owners, announcements_ids).run()
    except discord.errors.LoginFailure:
        console.print("Your token is invalid", "red")
