FROM python:3.6-stretch
WORKDIR /opt/bot
COPY requirements.txt .
RUN pip3 install -r requirements.txt
CMD ["python", "bot.py"]