# ![oculus-logo](oculus-logo-32x32.png) Oculus Bot

A Discord Bot with some cool commands

## Installation

**[Python 3.8.0](https://www.python.org/downloads/release/python-380/) is required!**

install [git](https://git-scm.com/downloads) and do ``git clone https://gitlab.com/Commandcracker/oculus-bot.git``\
or download the [zip](https://gitlab.com/Commandcracker/oculus-bot/-/archive/master/oculus-bot-master.zip) and unzip it with [7-Zip](https://www.7-zip.org/).

```bash
# use python3 or python depends on the installation

# install pip
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py

# download the requirements
python -m pip install -r requirements.txt
```

## Usage

Create a file called **token.txt** and paste your token in it

```bash
# use python3 or python depends on the installation
python bot.py
```

## Docker

If docker is installed you can build an image and run this as a container.

```bash
docker build -t Oculus-Bot-image .
```

Once the image is built, Oculus Bot can be used by running the following:

```bash
docker run --rm -t -v .:/opt/bot:rw Oculus-Bot-image -d
```

The optional ```--rm``` flag removes the container filesystem on completion to prevent cruft build-up.\
See: https://docs.docker.com/engine/reference/run/#clean-up---rm

The optional ```-t``` flag allocates a pseudo-TTY which allows colored output.\
See: https://docs.docker.com/engine/reference/run/#foreground

The optional ```-v``` flag allows to save the video on your Filesystem.\
See: https://docs.docker.com/storage/volumes/

The optional ```-d``` Run's container in background and print container ID\
See: https://docs.docker.com/engine/reference/commandline/run/
